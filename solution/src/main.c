#include "image_work.h"
#include "read_write.h"
#include "rotate.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
  if (argc < 3)
  {
    fprintf(stderr,  "You should write filename for input and output\n");
  }
  else
  {
    if (argv[1] != NULL)
    {
      FILE *file = fopen(argv[1], "rb");
      if (file == NULL)
      {
        fprintf(stderr,  "Impossible to open input file\n");
        return 1;
      }
      else
      {
        fprintf(stdout,  "Image successfully opened\n");

        struct image image = {0};

        enum read_status rs = from_bmp(file, &image);
        switch (rs)
        {
        case READ_OK:
          fprintf(stdout,  "read sucessfully\n");
          break;
        case READ_INVALID_HEADER:
          fprintf(stderr,  "faild to read bmp header\n");
          return 1;
          
        case READ_INVALID_SIGNATURE:
          fprintf(stderr,  "faild to load image because of wrong signature\n");
          return 1;
          
        case READ_INVALID_BITS:
          fprintf(stderr, "faild to load image because of wrong count of bits\n");
          return 1;
          
        case READ_PIXEL_LINE_ERROR:
          fprintf(stderr, "faild to load image while reading file\n");
          return 1;
          
        case SKEEP_TRUSH_ERROR:
          fprintf(stderr, "faild to load image while skeep trush\n");
          return 1;
          
        default:
          fprintf(stderr, "faild to load image, unknown error\n");
          return 1;
      
        }

        if (fclose(file) != 0) {fprintf(stderr, "faild to close input file\n");
        return 1;
        }

        struct image new_image = rotate(image);
        fprintf(stdout, "rotate finished\n");
        destroy_image(&image);
        fprintf(stdout,"pixels cleared\n");
       

        FILE *out = fopen(argv[2], "wb");
        if (out == NULL)
        {
          fprintf(stderr, "faild to open output file\n");
          return 1;
        }
        fprintf(stdout, "output file successfully opened\n");
        enum write_status ws = to_bmp(out, &new_image);

        switch (ws)
        {
        case WRITE_OK:
          fprintf(stdout, "successfully write image\n");
          break;
        case WRITE_ERROR_HEADER:
          fprintf(stderr, "faild to write header\n");
          return 1;;
        case WRITE_ERROR_PIXELS:
          fprintf(stderr, "faild to write pixels\n");
          return 1;;
        case WRITE_ERROR_PADDING:
          fprintf(stderr, "faild to write padding\n");
          return 1;;
        default:
          fprintf(stderr, "faild to write image, unknown error\n");
          return 1;;
        }
        if (fclose(out) != 0)
          {fprintf(stderr, "faild to close output file\n");
          return 1;}
        
        destroy_image(&new_image);
      }
    }
  }
  fprintf(stdout, "program finished work\n");
  return 0;
}
