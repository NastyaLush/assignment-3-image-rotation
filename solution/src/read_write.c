#include "bmp.h"
#include "image.h"
#include "image_work.h"
#include "read_write.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#define BFtype 19778
#define BFBITcount 24
#define BFreserved 0
#define BOffbits 54
#define BIsize 40
#define BIPlanes 1
#define BIComopression 0
#define BIXPelsPerMetr 2834
#define BIYPelsPerMetr 2834
#define BIcolorUsed 0
#define BIcolorImportent 0

static uint32_t get_padding_size(uint32_t width)
{
  return (4 - (width * 3) % 4) % 4;
}
enum read_status from_bmp(FILE *in, struct image *img)
{
  struct bmp_header bmp = {0};
  if (!fread(&bmp, sizeof(struct bmp_header), 1, in))
    return READ_INVALID_HEADER;

  // BM =19778
  if (bmp.bfType != BFtype)
    return READ_INVALID_SIGNATURE;

  if (bmp.biBitCount != BFBITcount)
    return READ_INVALID_BITS;

  *img = create_image(bmp.biWidth, bmp.biHeight);
  uint32_t padding = get_padding_size(img->width);
  for (size_t i = 0; i < bmp.biHeight; i++)
  {
    if (fread(img->data + i * bmp.biWidth, sizeof(struct pixel), bmp.biWidth, in) != bmp.biWidth)
      return READ_PIXEL_LINE_ERROR;
    if (fseek(in, padding, SEEK_CUR) != 0)
      return SKEEP_TRUSH_ERROR;
  }
  return READ_OK;
}


enum write_status to_bmp(FILE *out, struct image const *img)
{
  uint32_t padding = get_padding_size(img->width);
  struct bmp_header bmp_header = {0};
  bmp_header.bfType = BFtype;
  bmp_header.biSizeImage = (uint32_t)((sizeof(struct pixel) * img->width + padding) * img->height);
  bmp_header.bfileSize = bmp_header.biSizeImage + sizeof(struct bmp_header);
  bmp_header.bfReserved = BFreserved;
  bmp_header.bOffBits = BOffbits;
  bmp_header.biSize = BIsize;
  bmp_header.biWidth = (uint32_t)img->width;
  bmp_header.biHeight = (uint32_t)img->height;
  bmp_header.biPlanes = BIPlanes;
  bmp_header.biBitCount = BFBITcount;
  bmp_header.biCompression = BIComopression;
  bmp_header.biXPelsPerMeter = BIXPelsPerMetr;
  bmp_header.biYPelsPerMeter = BIYPelsPerMetr;
  bmp_header.biClrUsed = BIcolorUsed;
  bmp_header.biClrImportant = BIcolorImportent;

  size_t count = fwrite(&bmp_header, sizeof(struct bmp_header), 1, out);
  if (count != 1)
    return WRITE_ERROR_HEADER;
  for (size_t i = 0; i < img->height; i++)
  {
    count = fwrite(img->data + i * (img->width), sizeof(struct pixel), img->width, out);
    if (count != img->width)
      return WRITE_ERROR_PIXELS;

    count = fwrite("\0\0\0", padding, 1, out);
    if (count != 1) return WRITE_ERROR_PADDING;
  }
  return WRITE_OK;
}
